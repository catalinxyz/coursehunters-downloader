const puppeteer = require('puppeteer');
const download = require('download');
const fs = require('fs');

// const url = 'https://coursehunters.net/course/testirovanie-javascript';

const url = process.argv.slice(2)[0];

if (!url) {
  console.error('Pass a link to a course');
  process.exit(1);
}


(async () => {
  console.log('Fetching lessons list');
  const browser = await puppeteer.launch({headless: true, devtools: false});
  const page = await browser.newPage();
  await page.goto(url);

  const data = await page.evaluate(function() {
    return [...document.querySelectorAll('.lessons-list__li')].map(l => ({
        title: l.querySelector('span[itemprop=name]').textContent,
         url: l.querySelector('link[itemprop=contentUrl]').href
      }))
  });

  await browser.close();

  const downloadDir = 'courses'
  const courseName = url.split('/').pop();

  for (l of data) {
    console.log(`DOWNLOADING - ${l.title}`);
    await download(l.url, `${downloadDir}/${courseName}`, {
      filename: `${l.title}.mp4`
    });
  }

  console.log('DONE');
})();
